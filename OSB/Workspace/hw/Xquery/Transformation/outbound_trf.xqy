xquery version "1.0" encoding "Utf-8";

(: _input_schema=SimpleSchema :)
(: _output_schema= :)
(: _input_element=shipto :)
(: _output_element= :)
(: _is_complex=false :)
(: _type= :)
(: _shared_data=:)
(: _referenced_keys=:)

declare namespace mhs = "urn:com:aorta:pe:messageheader:v01";
declare namespace xf = "urn:aorta:outbound_trf";



declare function xf:outbound_trf($request as element(*))
    as element(*) {
        
            let $payload := $request//*:Payload
            return

		<AdapterMessage>
		  <ErrorCode>0</ErrorCode>
		  <ErrorMessage/>
		  <mhs:ObjectContext xmlns:mhs="urn:com:aorta:pe:messageheader:v01"/>
		  <payload>
		    <shipto>
		      <name>{data($request//*:shipto/*:name)}</name>
		      <address>{data($request//*:shipto/*:address)}</address>
		      <city>{data($request//*:shipto/*:city)}</city>
		      <country>{data($request//*:shipto/*:country)}</country>
		    </shipto>
		  </payload>
		</AdapterMessage>
};

declare variable $request as element(*) external;


xf:outbound_trf($request)
