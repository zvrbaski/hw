xquery version "1.0" encoding "Utf-8";

(: _input_schema= :)
(: _output_schema= :)
(: _input_element= :)
(: _output_element= :)
(: _is_complex=false :)
(: _type=response :)
(: _shared_data=:)
(: _referenced_keys=:)

declare namespace mhs = "urn:com:aorta:pe:messageheader:v01";
declare namespace xf = "urn:aorta:task_resp_trf";



declare function xf:task_resp_trf($request as element(*))
    as element(*) {
        
            let $payload := $request//*:Payload
            return

		<AdapterMessage>
		  <ErrorCode>0</ErrorCode>
		  <ErrorMessage/>
		  <mhs:ObjectContext xmlns:mhs="urn:com:aorta:pe:messageheader:v01"/>
		  <payload>{$payload}</payload>
		</AdapterMessage>
};

declare variable $request as element(*) external;


xf:task_resp_trf($request)
