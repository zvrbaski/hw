xquery version "1.0" encoding "utf-8";
(:: pragma  parameter="$request" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)

declare namespace xf = "http://tempuri.org/hw/Xquery/Transformation/Readers/hw_CustomReader/";
declare namespace urn = "urn:com:aorta:pe:aortamessage:v01";
declare namespace mhs = "urn:com:aorta:pe:messageheader:v01";
declare namespace phs = "urn:com:aorta:pe:processheader:v01";
declare namespace pld = "urn:com:aorta:pe:payload:v01";
declare namespace mtd = "urn:com:aorta:pe:messagetrackingdata:v01";

declare function xf:hw_CustomReader($request as element(*))
    as element(*) {

           let $requestID := concat("hw_","_",fn-bea:uuid())
        return
            <urn:AORTAMessage>
                            <urn:MessageType>EBO</urn:MessageType>
                         <urn:Version>0.1</urn:Version>
                            <mhs:MessageHeader>
                                <mhs:RefId/>
                                <mhs:RequestId>{ $requestID }</mhs:RequestId>
                                <mhs:MsgId/>
                                <mhs:RefDateTime xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
                                <mhs:ExternalId/>
                                <mhs:JobId/>
                            <mhs:BusinessEvent>hw</mhs:BusinessEvent>
                            <mhs:UseCase>hw</mhs:UseCase>
                                <mhs:Sender>
                                    <mhs:SenderCode>Nucleus</mhs:SenderCode>
                                    <mhs:CountryCode>NL</mhs:CountryCode>
                                    <mhs:Affiliate>Nucleus</mhs:Affiliate>
                                    <mhs:Instance>NL</mhs:Instance>
                            </mhs:Sender>
                                <mhs:ObjectReference>
                                    <mhs:ObjectName></mhs:ObjectName>
                                    <mhs:ObjectKeyName></mhs:ObjectKeyName>
                                    <mhs:ObjectKeyData>{ $requestID }</mhs:ObjectKeyData>
                                    <mhs:Domain>Aorta</mhs:Domain>
                                </mhs:ObjectReference>
                        </mhs:MessageHeader>
                            <phs:ProcessHeader>
                                <phs:ExecutionPlan>
                                    <phs:Version>1.0</phs:Version>
                                    <phs:Name>{concat("PS_","_Custom_Reader")}</phs:Name>
                                    <phs:ProcessTrackingLevel/>
                                    <phs:taskList>
                                        <phs:task>
                                            <phs:invoke taskDomain="OFF" taskName="CreateAORTAMessage" mep="sync">
                                                <phs:serviceTask>
                                                    <phs:taskSteps flow="request" technology="OSB">
                                                        <phs:transform location="hw/Xquery/Transformation/inbound_trf" type="OSB-Resource"/>
                                                        <phs:invoke protocol="HTTP" endpoint="[http_lb_osb]/ServiceBroker/Proxy_Service/PS_PE_ServiceBroker_Sync" systemName="AORTA" />
                                                    </phs:taskSteps>
                                                    <phs:taskSteps flow="response" technology="OSB">
                                                        <phs:transform location="hw/Xquery/Transformation/outbound_trf" type="OSB-Resource"/>
                                                    </phs:taskSteps>
                                                </phs:serviceTask>
                                            </phs:invoke>
                                        </phs:task>
                                    </phs:taskList>
                                </phs:ExecutionPlan>
                                <phs:ProcessAuditingLevel>2</phs:ProcessAuditingLevel>
                         </phs:ProcessHeader>
                            <pld:Payload>{ $request }</pld:Payload>
                            <mtd:MessageTrackingData/>
            </urn:AORTAMessage>
};

declare variable $request as element(*) external;

xf:hw_CustomReader($request)